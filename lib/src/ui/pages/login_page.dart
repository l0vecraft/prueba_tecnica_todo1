import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:geolocator/geolocator.dart';
import 'package:prueba_tecnica_todo1/src/core/constants/app_styles.dart';
import 'package:prueba_tecnica_todo1/src/core/cubits/auth/auth_cubit.dart';
import 'package:prueba_tecnica_todo1/src/core/cubits/weather/weather_cubit.dart';
import 'package:prueba_tecnica_todo1/src/core/model/login_model.dart';
import 'package:prueba_tecnica_todo1/src/core/validators/validator.dart';
import 'package:prueba_tecnica_todo1/src/ui/widgets/custom_button.dart';
import 'package:prueba_tecnica_todo1/src/ui/widgets/custom_progress_indicator.dart';
import 'package:prueba_tecnica_todo1/src/ui/widgets/fields/custom_form_field.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  var _username = TextEditingController();
  var _password = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  Position? _position;
  bool? _serviceEnabled;

  @override
  void initState() {
    super.initState();
    _getLocation();
  }

  void _getLocation() async {
    _serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!_serviceEnabled!) {
      var permission = await Geolocator.checkPermission();
      if (permission == LocationPermission.denied ||
          permission == LocationPermission.deniedForever) {
        Future.error('Debe habilitar los permisos de localizacion');
      }
    }
    _position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high,
        forceAndroidLocationManager: true);
    context.read<WeatherCubit>().position = _position!;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        alignment: Alignment.center,
        child: GestureDetector(
          onTap: () {
            FocusNode _focus = FocusScope.of(context);
            if (!_focus.hasPrimaryFocus) _focus.unfocus();
          },
          child: BlocConsumer<AuthCubit, AuthState>(
            listener: (context, state) {
              if (state is AuthLoading) {
                showDialog(
                    context: context,
                    builder: (context) => Dialog(
                          child: Container(
                              height: 100.h,
                              width: 100.w,
                              child: CustomProgressIndicator()),
                        ));
              }
              if (state is AuthLoaded) {
                Navigator.pop(context);
                Navigator.pushNamedAndRemoveUntil(
                    context, 'home', (route) => false);
              }
              if (state is AuthError) {
                Navigator.pop(context);
                showDialog(
                    context: context,
                    builder: (context) => Dialog(
                          child: Container(
                            height: 100.h,
                            width: 100.w,
                            child: Center(
                              child: Text(state.message.message),
                            ),
                          ),
                        ));
              }
            },
            builder: (context, state) {
              return Form(
                key: _formKey,
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ShaderMask(
                        shaderCallback: (rect) {
                          return LinearGradient(
                                  colors: [
                                Color(0xfff1c4a4),
                                Color(0xFFCC9D7B),
                              ],
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter)
                              .createShader(rect);
                        },
                        blendMode: BlendMode.srcIn,
                        child: FlutterLogo(
                          size: 110.h,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 20.h),
                        child: Text(
                          'Login Page',
                          style: AppStyles.title1,
                        ),
                      ),
                      CustomFormField(
                        controller: _username,
                        text: 'Nombre de usuario',
                        onSaved: (String value) {
                          setState(() {
                            _username.text = value.trim();
                          });
                        },
                        onSubmited: (String value) {
                          setState(() {
                            _username.text = value.trim();
                          });
                        },
                        validator: (value) {
                          return Validators.requiredField(value);
                        },
                      ),
                      CustomFormField(
                        controller: _password,
                        text: 'Contraseña',
                        obscure: true,
                        onSaved: (String value) {
                          setState(() {
                            _password.text = value.trim();
                          });
                        },
                        onSubmited: (value) {
                          setState(() {
                            _password.text = value.trim();
                          });
                        },
                        validator: (value) => Validators.requiredField(value),
                      ),
                      SizedBox(height: 30.h),
                      BlocBuilder<AuthCubit, AuthState>(
                        builder: (context, state) {
                          return CustomButton(
                              onPress: () {
                                if (_formKey.currentState!.validate()) {
                                  _formKey.currentState!.save();
                                  var user = LoginModel(
                                      username: _username.text,
                                      password: _password.text);
                                  context.read<AuthCubit>().authUser(user);
                                }
                              },
                              title: 'LOGIN',
                              color: Color(0xfff1c4a4));
                        },
                      ),
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}

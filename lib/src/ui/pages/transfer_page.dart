import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:prueba_tecnica_todo1/src/core/constants/app_styles.dart';
import 'package:prueba_tecnica_todo1/src/core/model/transfer_model.dart';
import 'package:prueba_tecnica_todo1/src/core/validators/validator.dart';
import 'package:prueba_tecnica_todo1/src/ui/widgets/fields/custom_form_field.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:qr_flutter/qr_flutter.dart';

class TransferPage extends StatefulWidget {
  const TransferPage({Key? key}) : super(key: key);

  @override
  _TransferPageState createState() => _TransferPageState();
}

class _TransferPageState extends State<TransferPage> {
  final _formKey = GlobalKey<FormState>();
  var _cuenta = TextEditingController();
  var _monto = TextEditingController();

  Widget _generateQR(TransferModel transfer) {
    return Container(
      alignment: Alignment.center,
      child: QrImage(
        data: transfer.toString(),
        version: QrVersions.auto,
        size: 260.h,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 2,
          leading: IconButton(
              onPressed: () => Navigator.pop(context),
              icon: Icon(
                Icons.arrow_back,
                color: Color(0xfff1c4a4),
              ))),
      body: GestureDetector(
        onTap: () {
          FocusNode _currentFocuts = FocusScope.of(context);
          if (!_currentFocuts.hasPrimaryFocus) _currentFocuts.unfocus();
        },
        child: Container(
          child: Form(
              key: _formKey,
              child: ListView(
                children: [
                  Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 20.w, vertical: 18.h),
                    child:
                        Text('Enviar Transferencia', style: AppStyles.title1),
                  ),
                  CustomFormField(
                    controller: _cuenta,
                    keyboard: TextInputType.number,
                    text: 'Ingrese cuenta destino',
                    maxLength: 12,
                    validator: (value) => Validators.requiredField(value),
                    onSubmited: (value) {
                      setState(() {
                        _cuenta.text = value;
                      });
                    },
                    onSaved: (value) {
                      setState(() {
                        _cuenta.text = value;
                      });
                    },
                  ),
                  CustomFormField(
                    controller: _monto,
                    keyboard: TextInputType.number,
                    text: 'Ingrese el monto a enviar',
                    maxLength: 8,
                    validator: (value) => Validators.requiredField(value),
                    onSubmited: (value) {
                      setState(() {
                        _monto.text = value;
                      });
                    },
                    onSaved: (value) {
                      setState(() {
                        _monto.text = value;
                      });
                    },
                  )
                ],
              )),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: Padding(
          padding: EdgeInsets.only(bottom: 10.h),
          child: SlideInUp(
            duration: Duration(milliseconds: 450),
            child: Container(
              height: 80.h,
              width: 80.w,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Color(0xfff1c4a4),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey,
                        blurRadius: 10,
                        spreadRadius: 1,
                        offset: Offset(0, 4))
                  ]),
              child: InkWell(
                onTap: () {
                  if (_formKey.currentState!.validate()) {
                    var data = TransferModel(
                        cuenta: _cuenta.text,
                        cantidad: double.parse(_monto.text),
                        moneda: 'co');
                    showModalBottomSheet(
                        context: context,
                        builder: (context) => _generateQR(data));
                  }
                },
                customBorder: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50)),
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        FontAwesomeIcons.qrcode,
                        size: 32.sp,
                      ),
                      SizedBox(height: 5.h),
                      Text(
                        'Generar',
                        style: AppStyles.miniTitle1,
                      )
                    ],
                  ),
                ),
              ),
            ),
          )),
    );
  }
}

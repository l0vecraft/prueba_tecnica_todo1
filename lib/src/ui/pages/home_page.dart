import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:prueba_tecnica_todo1/src/core/constants/app_styles.dart';
import 'package:prueba_tecnica_todo1/src/core/cubits/auth/auth_cubit.dart';
import 'package:prueba_tecnica_todo1/src/core/cubits/movement/movement_cubit.dart';
import 'package:prueba_tecnica_todo1/src/core/cubits/weather/weather_cubit.dart';
import 'package:prueba_tecnica_todo1/src/ui/widgets/account/card_account.dart';
import 'package:prueba_tecnica_todo1/src/ui/widgets/account/movement_tile.dart';
import 'package:prueba_tecnica_todo1/src/ui/widgets/menu/menu_options.dart';
import 'package:prueba_tecnica_todo1/src/ui/widgets/custom_progress_indicator.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var _currentPage = 0;
  late PageController _pageController;

  @override
  void initState() {
    super.initState();
    context.read<MovementCubit>().getData();
    context.read<WeatherCubit>().getWeather();
    _pageController =
        PageController(initialPage: _currentPage, viewportFraction: .8);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(child: BlocBuilder<MovementCubit, MovementState>(
          builder: (context, state) {
            if (state is MovementLoaded) {
              return Stack(
                children: [
                  Container(
                    color: Colors.black,
                    child: PageView.builder(
                        controller: _pageController,
                        onPageChanged: (value) {
                          setState(() {
                            _currentPage = value;
                          });
                        },
                        itemCount: state.accounts.length,
                        itemBuilder: (context, i) => CardAccount(
                              account: state.accounts[i],
                            )),
                  ),
                  Positioned(
                    bottom: 0,
                    child: Container(
                      decoration: AppStyles.borderRadius,
                      height: 430.h,
                      width: MediaQuery.of(context).size.width,
                      child: CustomScrollView(
                        slivers: [
                          SliverToBoxAdapter(
                            child: MenuOptions(),
                          ),
                          SliverToBoxAdapter(
                            child: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 28.w),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('Ultimos movimientos',
                                      style: AppStyles.miniTitle1
                                          .copyWith(color: Colors.grey)),
                                  Divider()
                                ],
                              ),
                            ),
                          ),
                          SliverPadding(
                            padding: EdgeInsets.symmetric(horizontal: 28.w),
                            sliver: SliverList(
                                delegate: SliverChildListDelegate(state
                                    .accounts[_currentPage].movements.reversed
                                    .map((e) => SlideInLeft(
                                        child: MovementTile(movement: e)))
                                    .toList())),
                          )
                        ],
                      ),
                    ),
                  ),
                  BlocBuilder<AuthCubit, AuthState>(
                    builder: (context, state) {
                      if (state is AuthLoaded) {
                        return Positioned(
                          top: 20.h,
                          left: 15.w,
                          child: Row(
                            children: [
                              Text(
                                'Hola, ${state.username}',
                                overflow: TextOverflow.ellipsis,
                                textAlign: TextAlign.left,
                                style: AppStyles.title2,
                              ),
                              SizedBox(width: 60.w),
                              BlocBuilder<WeatherCubit, WeatherState>(
                                builder: (context, state) {
                                  if (state is WeatherLoaded) {
                                    var temp =
                                        (state.weather.main!.temp - 273.15)
                                            .ceil();
                                    return Row(
                                      children: [
                                        Text(
                                          '$temp° ${state.weather.name}',
                                          overflow: TextOverflow.ellipsis,
                                          textAlign: TextAlign.left,
                                          style: AppStyles.title2,
                                        ),
                                        Image.network(
                                            'http://openweathermap.org/img/w/${state.weather.weather!.first.icon}.png')
                                      ],
                                    );
                                  } else {
                                    return Text('tiempo no disponible',
                                        style: AppStyles.title2);
                                  }
                                },
                              ),
                            ],
                          ),
                        );
                      } else {
                        return Container();
                      }
                    },
                  )
                ],
              );
            }
            return CustomProgressIndicator();
          },
        )),
      ),
    );
  }
}

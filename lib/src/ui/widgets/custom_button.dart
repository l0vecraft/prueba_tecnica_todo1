import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:prueba_tecnica_todo1/src/core/constants/app_styles.dart';

class CustomButton extends StatelessWidget {
  final VoidCallback onPress;
  final Color color;
  final String title;

  const CustomButton(
      {Key? key,
      required this.onPress,
      required this.title,
      this.color = Colors.blue})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: RawMaterialButton(
        onPressed: onPress,
        fillColor: color,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 10.h, horizontal: 100.w),
          child: Text('$title', style: AppStyles.button1),
        ),
      ),
    );
  }
}

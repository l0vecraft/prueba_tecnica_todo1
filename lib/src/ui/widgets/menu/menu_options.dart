import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:prueba_tecnica_todo1/src/core/cubits/movement/movement_cubit.dart';
import 'package:prueba_tecnica_todo1/src/ui/widgets/menu/menu_item.dart';

class MenuOptions extends StatefulWidget {
  const MenuOptions({Key? key}) : super(key: key);

  @override
  _MenuOptionsState createState() => _MenuOptionsState();
}

class _MenuOptionsState extends State<MenuOptions> {
  Future _scanQR(BuildContext context) async {
    String? scanResult;
    try {
      scanResult = await FlutterBarcodeScanner.scanBarcode(
          '#ff6666', 'Cancel', true, ScanMode.QR);
    } catch (e) {
      print(e);
    }
    if (!mounted) return;
    context.read<MovementCubit>().addMoney(35000);
    print('se agregaron 35K');
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 40, horizontal: 30.w),
      child: Container(
        alignment: Alignment.center,
        height: 90.h,
        decoration: BoxDecoration(
            border: Border.all(color: Colors.grey),
            borderRadius: BorderRadius.circular(18)),
        child: BlocBuilder<MovementCubit, MovementState>(
          builder: (context, state) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                MenuItem(
                    icon: FontAwesomeIcons.moneyBillWave,
                    title: 'Transferencia',
                    onPress: () {
                      Navigator.pushNamed(context, 'transfer');
                    }),
                MenuItem(
                    icon: FontAwesomeIcons.qrcode,
                    title: 'Leer QR',
                    onPress: () => _scanQR(context)),
                // MenuItem(
                //     icon: FontAwesomeIcons.creditCard,
                //     title: 'Detalles',
                //     onPress: () {}),
              ],
            );
          },
        ),
      ),
    );
  }
}

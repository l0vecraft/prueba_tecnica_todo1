import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:prueba_tecnica_todo1/src/core/constants/app_styles.dart';

class MenuItem extends StatelessWidget {
  final IconData icon;
  final String title;
  final VoidCallback onPress;
  const MenuItem(
      {Key? key,
      required this.icon,
      required this.title,
      required this.onPress})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPress,
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(icon, size: 30.sp),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 8.h),
              child: Text(title, style: AppStyles.miniTitle1),
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:prueba_tecnica_todo1/src/core/constants/app_styles.dart';
import 'package:prueba_tecnica_todo1/src/core/model/account_model.dart';

import 'package:intl/intl.dart';

class CardAccount extends StatelessWidget {
  final AccountModel account;
  const CardAccount({Key? key, required this.account}) : super(key: key);

  static var _formater = NumberFormat('###,###,###,###.##', 'en_US');

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: 500.h, left: 5.w, right: 8.w, top: 80.h),
      child: Container(
        height: 50.h,
        width: 120.w,
        padding: EdgeInsets.symmetric(horizontal: 15.w, vertical: 8.h),
        decoration: AppStyles.cardDecoration,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              '${account.type}',
              style: AppStyles.cardTitle1,
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 12.h),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Balance', style: AppStyles.cardTitle2),
                  Text(
                    '\$ ${_formater.format(
                      account.balance,
                    )}',
                    style: AppStyles.cardTitle2,
                  )
                ],
              ),
            ),
            SizedBox(height: 13.h),
            Center(
              child: Text(
                '1234 5678 9012 3456',
                style: AppStyles.accountNumber,
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 17.h),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('05/24', style: AppStyles.dateCard),
                  Icon(
                    FontAwesomeIcons.ccVisa,
                    size: 40.sp,
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:prueba_tecnica_todo1/src/core/constants/app_styles.dart';
import 'package:prueba_tecnica_todo1/src/core/model/movement_model.dart';

class MovementTile extends StatefulWidget {
  final MovementModel movement;
  const MovementTile({Key? key, required this.movement}) : super(key: key);

  @override
  _MovementTileState createState() => _MovementTileState();
}

class _MovementTileState extends State<MovementTile> {
  var _formater = NumberFormat('###,###,###,###.##', 'en_US');
  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Icon(
        widget.movement.type == 'in'
            ? FontAwesomeIcons.chartLine
            : FontAwesomeIcons.minus,
        color: widget.movement.type == 'in' ? Colors.green : Colors.red,
      ),
      title: Text(widget.movement.type == 'in' ? 'Ingreso' : 'Gasto'),
      trailing: Text(
        widget.movement.type == 'in'
            ? '\$ ${_formater.format(widget.movement.amount)}'
            : '\$ - ${_formater.format(widget.movement.amount)}',
        style: AppStyles.miniTitle1.copyWith(
            color: widget.movement.type == 'in' ? Colors.green : Colors.red),
      ),
    );
  }
}

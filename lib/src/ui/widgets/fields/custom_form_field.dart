import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:prueba_tecnica_todo1/src/core/constants/app_styles.dart';

class CustomFormField extends StatelessWidget {
  final TextEditingController controller;
  final String text;
  final TextInputType keyboard;
  final Function? validator;
  final Function? onSaved;
  final Function? onSubmited;
  final int? maxLength;
  final bool obscure;
  const CustomFormField(
      {Key? key,
      required this.controller,
      required this.text,
      this.validator,
      this.obscure = false,
      this.onSaved,
      this.keyboard = TextInputType.name,
      this.onSubmited,
      this.maxLength})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10.h, horizontal: 18.w),
      child: TextFormField(
        controller: controller,
        obscureText: obscure,
        style: AppStyles.fieldStyle,
        cursorColor: Color(0xfff1c4a4),
        keyboardType: obscure ? TextInputType.visiblePassword : keyboard,
        maxLength: maxLength ?? null,
        decoration: InputDecoration(
            labelText: text,
            labelStyle: TextStyle(color: Colors.black),
            border: AppStyles.circularBorder,
            enabledBorder: AppStyles.circularBorder,
            focusedBorder: AppStyles.circularBorder),
        validator: (value) => validator!(value),
        onFieldSubmitted: (value) => onSaved!(value),
        onSaved: (value) => onSubmited!(value),
      ),
    );
  }
}

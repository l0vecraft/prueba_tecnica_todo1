import 'package:equatable/equatable.dart';

class LoginModel extends Equatable {
  final String username;
  final String password;

  LoginModel({required this.username, required this.password});

  @override
  String toString() => 'LoginModel(username: $username, password: $password)';

  @override
  List<Object?> get props => [this.username, this.password];
}

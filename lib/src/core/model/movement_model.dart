import 'package:equatable/equatable.dart';

class MovementModel extends Equatable {
  final String type;
  final double amount;
  MovementModel({
    required this.type,
    required this.amount,
  });
  @override
  List<Object> get props => [type, amount];

  @override
  bool get stringify => true;
}

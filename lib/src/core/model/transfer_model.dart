import 'package:equatable/equatable.dart';

class TransferModel extends Equatable {
  final String cuenta;
  final double cantidad;
  final String moneda;
  TransferModel({
    required this.cuenta,
    required this.cantidad,
    required this.moneda,
  });

  @override
  List<Object> get props => [cuenta, cantidad, moneda];

  @override
  bool get stringify => true;
}

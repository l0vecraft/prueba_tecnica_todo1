import 'package:equatable/equatable.dart';
import 'package:prueba_tecnica_todo1/src/core/model/account_model.dart';
import 'package:prueba_tecnica_todo1/src/core/model/movement_model.dart';

class UserModel extends Equatable {
  final String username;
  final List<AccountModel> accounts;
  UserModel({
    required this.username,
    required this.accounts,
  });

  @override
  List<Object?> get props => [this.accounts, this.username];
}

UserModel fakeReponse = UserModel(username: 'prueba_user', accounts: [
  AccountModel(
      type: 'Ahorros',
      movements: [
        MovementModel(type: 'in', amount: 200000),
        MovementModel(type: 'in', amount: 205000),
        MovementModel(type: 'out', amount: 40000),
        MovementModel(type: 'in', amount: 33450),
        MovementModel(type: 'out', amount: 58206),
        MovementModel(type: 'out', amount: 68403),
      ],
      balance: 1283719),
  AccountModel(
      type: 'Corriente',
      movements: [
        MovementModel(type: 'in', amount: 100000),
        MovementModel(type: 'in', amount: 305000),
        MovementModel(type: 'out', amount: 47000),
        MovementModel(type: 'in', amount: 53450),
        MovementModel(type: 'out', amount: 18206),
        MovementModel(type: 'out', amount: 88403),
      ],
      balance: 4729128)
]);

import 'package:equatable/equatable.dart';
import 'package:prueba_tecnica_todo1/src/core/model/movement_model.dart';

class AccountModel extends Equatable {
  final String type;
  final double balance;
  final List<MovementModel> movements;
  AccountModel({
    required this.balance,
    required this.type,
    required this.movements,
  });
  @override
  List<Object?> get props => [this.movements, this.type];
}

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';

class AppStyles {
  static var title1 = GoogleFonts.quicksand(fontSize: 28.sp);
  static var title2 =
      GoogleFonts.quicksand(color: Colors.white, fontSize: 15.sp);
  static var miniTitle1 = GoogleFonts.quicksand(fontWeight: FontWeight.w700);
  static var button1 = GoogleFonts.quicksand(
      fontSize: 22.sp, fontWeight: FontWeight.w600, letterSpacing: 2.w);

  /*FIELDS*/
  static OutlineInputBorder circularBorder =
      OutlineInputBorder(borderRadius: BorderRadius.circular(30));
  static var fieldStyle = GoogleFonts.quicksand();

/*CARD */
  static var cardTitle1 =
      GoogleFonts.quicksand(fontSize: 25.sp, fontWeight: FontWeight.bold);
  static var cardTitle2 = GoogleFonts.shareTechMono(
    fontSize: 16.sp,
  );
  static var accountNumber = GoogleFonts.shareTechMono(fontSize: 20.sp);
  static var dateCard = GoogleFonts.shareTechMono(fontSize: 15.sp);

  /* CONTAINERS */
  static var borderRadius = BoxDecoration(
    borderRadius: BorderRadius.only(
        topLeft: Radius.circular(35), topRight: Radius.circular(35)),
    color: Colors.white,
  );
  static var cardDecoration = BoxDecoration(
      color: Color(0xfff1c4a4), borderRadius: BorderRadius.circular(18));
}

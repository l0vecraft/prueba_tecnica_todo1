import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:geolocator/geolocator.dart';
import 'package:prueba_tecnica_todo1/src/core/model/exceptions/failure.dart';
import 'package:prueba_tecnica_todo1/src/core/model/weather_model.dart';
import 'package:prueba_tecnica_todo1/src/data/repository/weather_repository.dart';

part 'weather_state.dart';

class WeatherCubit extends Cubit<WeatherState> {
  final WeatherRepository api;
  WeatherCubit({required this.api}) : super(WeatherInitial());

  Position? _position;

  set position(Position value) {
    _position = value;
  }

  void getWeather() async {
    emit(WeatherLoading());
    try {
      var res = await api.getCurrentWeather(_position!);

      emit(WeatherLoaded(weather: res));
    } on Failure catch (e) {
      emit(WeatherError(message: e));
    }
  }
}

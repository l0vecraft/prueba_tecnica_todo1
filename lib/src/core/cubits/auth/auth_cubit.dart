import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:prueba_tecnica_todo1/src/core/cubits/movement/movement_cubit.dart';
import 'package:prueba_tecnica_todo1/src/core/model/exceptions/failure.dart';
import 'package:prueba_tecnica_todo1/src/core/model/login_model.dart';
import 'package:prueba_tecnica_todo1/src/core/model/user_model.dart';
import 'package:prueba_tecnica_todo1/src/data/repository/auth_repository.dart';

part 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  final AuthRepository api;
  AuthCubit({required this.api}) : super(AuthInitial());

  void authUser(LoginModel user) async {
    emit(AuthLoading());
    try {
      var response = await api.authUser(user);
      emit(AuthLoaded(username: response.username));
    } on Failure catch (e) {
      emit(AuthError(message: e));
    }
  }
}

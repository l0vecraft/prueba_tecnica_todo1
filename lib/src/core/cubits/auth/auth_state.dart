part of 'auth_cubit.dart';

abstract class AuthState extends Equatable {
  const AuthState();

  @override
  List<Object> get props => [];
}

class AuthInitial extends AuthState {}

class AuthLoading extends AuthState {}

class AuthLoaded extends AuthState {
  final String username;
  AuthLoaded({required this.username});
}

class AuthError extends AuthState {
  final Failure message;
  AuthError({required this.message});
}

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:prueba_tecnica_todo1/src/core/model/account_model.dart';
import 'package:prueba_tecnica_todo1/src/data/network/fake/fake_api_account.dart';

part 'movement_state.dart';

class MovementCubit extends Cubit<MovementState> {
  final FakeApiAccount api;
  MovementCubit({required this.api}) : super(MovementInitial());

  void getData() async {
    emit(MovementLoading());
    var res = await api.getData();
    emit(MovementLoaded(accounts: res));
  }

  void addMoney(double amount) async {
    emit(MovementLoading());
    await api.addMovement(amount);
    getData();
  }

  void makeTransfer(double amount) async {
    await api.makeTransfer(amount);
  }
}

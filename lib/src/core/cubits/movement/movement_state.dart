part of 'movement_cubit.dart';

abstract class MovementState extends Equatable {
  const MovementState();

  @override
  List<Object> get props => [];
}

class MovementInitial extends MovementState {}

class MovementLoading extends MovementState {}

class MovementLoaded extends MovementState {
  final List<AccountModel> accounts;
  MovementLoaded({required this.accounts});
}

class MovementError extends MovementState {}

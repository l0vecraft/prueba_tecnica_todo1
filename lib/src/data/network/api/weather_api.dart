import 'package:dio/dio.dart';
import 'package:geolocator_platform_interface/src/models/position.dart';
import 'package:prueba_tecnica_todo1/src/core/constants/variables.dart';
import 'package:prueba_tecnica_todo1/src/core/model/exceptions/failure.dart';
import 'package:prueba_tecnica_todo1/src/core/model/weather_model.dart';
import 'package:prueba_tecnica_todo1/src/data/repository/weather_repository.dart';

class WeatherApi implements WeatherRepository {
  var _urlBase = 'http://api.openweathermap.org/data/2.5/weather';
  var _dio = Dio();

  @override
  Future getCurrentWeather(Position location) async {
    var weatherResponse = WeatherModel();
    try {
      var res = await _dio.get(
          '$_urlBase?lat=${location.latitude}&lon=${location.longitude}&&appid=$weatherKey');
      if (res != null) {
        weatherResponse = WeatherModel.fromMap(res.data);
      }
      print(res);
    } on Exception {
      throw Failure(
          message: 'Error, no se pudieron obtener los datos del clima');
    }
    return weatherResponse;
  }
}

import 'package:prueba_tecnica_todo1/src/core/model/account_model.dart';
import 'package:prueba_tecnica_todo1/src/core/model/exceptions/failure.dart';
import 'package:prueba_tecnica_todo1/src/core/model/login_model.dart';
import 'package:prueba_tecnica_todo1/src/core/model/user_model.dart';
import 'package:prueba_tecnica_todo1/src/data/repository/auth_repository.dart';

class FakeApiAuth implements AuthRepository {
  @override
  Future<UserModel> authUser(LoginModel user) async {
    UserModel? response;
    Future.delayed(Duration(seconds: 2));
    try {
      if (user.username == 'prueba' && user.password == '1234') {
        response = fakeReponse;
      } else {
        throw Failure(message: 'Error al iniciar sesion, verifique los datos');
      }
    } on Exception {
      throw Failure(message: 'Error al iniciar sesion, verifique los datos');
    }
    return response;
  }
}

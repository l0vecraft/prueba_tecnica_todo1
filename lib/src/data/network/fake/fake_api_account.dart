import 'package:prueba_tecnica_todo1/src/core/model/account_model.dart';
import 'package:prueba_tecnica_todo1/src/core/model/movement_model.dart';
import 'package:prueba_tecnica_todo1/src/core/model/user_model.dart';
import 'package:prueba_tecnica_todo1/src/data/repository/account_repository.dart';

class FakeApiAccount implements AccountRepository {
  @override
  Future<void> addMovement(double amount) async {
    Future.delayed(Duration(seconds: 2));
    fakeReponse.accounts.first.movements
        .add(MovementModel(type: 'in', amount: amount));
  }

  @override
  Future<void> makeTransfer(double amount) async {
    Future.delayed(Duration(seconds: 2)).then((value) => fakeReponse
        .accounts.first.movements
        .add(MovementModel(type: 'out', amount: amount)));
  }

  @override
  Future<List<AccountModel>> getData() {
    Future.delayed(Duration(seconds: 2));
    return Future.value(fakeReponse.accounts);
  }
}

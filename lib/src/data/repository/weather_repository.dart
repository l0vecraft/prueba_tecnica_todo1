import 'package:geolocator/geolocator.dart';

abstract class WeatherRepository {
  Future getCurrentWeather(Position location);
}

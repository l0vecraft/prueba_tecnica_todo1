import 'package:prueba_tecnica_todo1/src/core/model/login_model.dart';
import 'package:prueba_tecnica_todo1/src/core/model/user_model.dart';

abstract class AuthRepository {
  Future<UserModel> authUser(LoginModel user);
}

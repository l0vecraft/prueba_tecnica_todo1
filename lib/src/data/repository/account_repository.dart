import 'package:prueba_tecnica_todo1/src/core/model/account_model.dart';

abstract class AccountRepository {
  Future<void> addMovement(double amount);
  Future<void> makeTransfer(double amount);
  Future<List<AccountModel>> getData();
}

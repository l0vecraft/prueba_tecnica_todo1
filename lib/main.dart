import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:prueba_tecnica_todo1/routes.dart';
import 'package:prueba_tecnica_todo1/src/core/cubits/auth/auth_cubit.dart';
import 'package:prueba_tecnica_todo1/src/core/cubits/movement/movement_cubit.dart';
import 'package:prueba_tecnica_todo1/src/core/cubits/weather/weather_cubit.dart';
import 'package:prueba_tecnica_todo1/src/data/network/api/weather_api.dart';
import 'package:prueba_tecnica_todo1/src/data/network/fake/fake_api_account.dart';
import 'package:prueba_tecnica_todo1/src/data/network/fake/fake_api_auth.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<AuthCubit>(
          create: (context) => AuthCubit(api: FakeApiAuth()),
        ),
        BlocProvider<MovementCubit>(
          create: (context) => MovementCubit(api: FakeApiAccount()),
        ),
        BlocProvider<WeatherCubit>(
          create: (context) => WeatherCubit(api: WeatherApi()),
        )
      ],
      child: ScreenUtilInit(
        designSize: Size(375, 812),
        builder: () => MaterialApp(
          debugShowCheckedModeBanner: false,
          onGenerateRoute: Routes.generateRoute,
          initialRoute: '/',
        ),
      ),
    );
  }
}

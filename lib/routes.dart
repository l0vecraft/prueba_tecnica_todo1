import 'package:flutter/material.dart';
import 'package:prueba_tecnica_todo1/src/ui/pages/details_page.dart';
import 'package:prueba_tecnica_todo1/src/ui/pages/home_page.dart';
import 'package:prueba_tecnica_todo1/src/ui/pages/login_page.dart';
import 'package:prueba_tecnica_todo1/src/ui/pages/transfer_page.dart';

class Routes {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (context) => LoginPage());
      case 'home':
        return MaterialPageRoute(builder: (context) => HomePage());
      case 'details':
        return MaterialPageRoute(builder: (context) => DetailsPage());
      case 'transfer':
        return MaterialPageRoute(builder: (context) => TransferPage());
      default:
        return MaterialPageRoute(builder: (context) => LoginPage());
    }
  }
}
